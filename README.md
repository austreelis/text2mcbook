# Text to Minecraft book tool

This project aims at providing a simple to use tool to generate well formated
text for minecraft books from raw text file.

Currently, only a basic implementation in python is available. In the near
future, I plan on doing a complete program, and packaged executable for Linux
and Windows.

Except when explicitly specified, any source/non-binary file in this repo
is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the text files of this repo under the terms of the
CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL
[http://www.cecill.info](http://www.cecill.info).
