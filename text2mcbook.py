#!/bin/usr/python3

"""
Copyright © Morgan Leclerc (2019)

morgan.leclerc@caramail.com

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
"""

__author__ = 'Morgan Leclerc'

if __name__ == '__main__':
    from argparse import ArgumentParser
    import textwrap

    arg_parser = ArgumentParser(prog='text2mcbook', description='This tool'
                                'makes it easy to evaluate the length of your '
                                'text into minecraft books, and to copy-paste '
                                'it into them (since minecraft 1.14).')
    arg_parser.add_argument('input', type=str, help='The input'
                             'file')
    arg_parser.add_argument('-o', '--out', dest='output', type=str, help='The'
                            'name of the output file. Modify input file if not'
                            'specified.')
    arg_parser.add_argument('-u', '--unicode-font', dest='unicode',
                            action='store_true', help='Will consider the reader'
                            ' have enabled unicode fonts in their game.')

    args = arg_parser.parse_args()

    inp_file = out_file = args.input
    if args.output is not None:
        out_file = args.output

    # Reads input file
    text = None
    with open(inp_file, 'r') as input:
        text = input.read()
    if text is None:
        raise IOError(f'Could not read all content of {inp_file}')

    # Accumulates lines into paragraphs and mark up line breaks and paragraph breaks
    lines = text.split('\n')
    text = ''
    for line in lines:
        if line:
            text += line.strip() + '\x91'
        else:
            text += '\x92'

    # Splits lines to match minecraft's books line length preserving paragraphs
    # 19 except 18 on last page's line (no unicode font)
    # 29 except 27 on last page's line (unicode font)
    length = 29 if args.unicode else 19
    par_len = 10 if args.unicode else 15
    paragraphs = text.split('\x92')
    line_count = 0
    text = []
    for par in paragraphs:
        lines = par.split('\x91')
        for line in lines:
            while line:
                w_line = line[:2*length]
                text.append(textwrap.wrap(w_line, width=length-(line_count==par_len-1))[0])
                line = line[len(text[-1])+1:]
                line_count = (line_count+1) % par_len
        text.append('')

    # Gather lines in pages (13 by page)
    r_text = list(reversed(text))
    text = []
    while r_text:
        page = list(reversed(r_text[-13:]))
        for _ in range(len(page)):
            r_text.pop()
        while r_text and not r_text[-1]:
            r_text.pop()
        text.append('\n'.join(page) + '\n')

    # Gather pages in books (50 by book)
    r_text = list(reversed(text))
    text = []
    while r_text:
        book = list(reversed(r_text[-50:]))
        for _ in range(len(book)):
            r_text.pop()
        while r_text and not r_text[-1]:
            r_text.pop()
        text.append(book)

    # Write text tou output file
    with open(out_file, 'w') as output:
        for bi, book in enumerate(text):
            for pi, page in enumerate(book):
                output.write(f'\n###--- BOOK {bi+1} PAGE {pi+1} ---###\n\n')
                output.write(page)
